---
title: "Présentation du module Projets"
date: 2016-05-18
---

# Objectifs du module

- exercice de développement à peu près réaliste
- travail sur un thème/sujet donné (demande client)
- travail en équipe
- travail en mode projet (méthode, outils)

# Organisation

- du 6 au 24 juin 2016 à temps plein
- présence **OBLIGATOIRE** : 9h-12h + 13h-16h
- travail en autonomie (developpement, conduite de projet) + aide régulière des encadrants + CM
- à rendre : cahier des charges, livrables, rapport, soutenance
- informations : [https://www-lisic.univ-littoral.fr/~dehos](https://www-lisic.univ-littoral.fr/~dehos)

# Travail à faire avant le début des projets

- essayer de faire des groupes (3 ou 4 étudiants)
- classer les thèmes de projet par ordre de préférence du groupe

# Thème 1 (Éric Ramat)

- développement d’une plate-forme Web pour le jeu de cartes Pokémon
- points importants :
    - architecture d’une application Web entièrement basée sur Javascript (client ET serveur)
    - intégration dans une architecture existante (nodejs + express + jquery + booststrap)
    - développement du moteur de jeu
    - création d'une base de données mongodb pour les cartes
    - interface de jeu selon [la règle du jeu suivante](http://assets.pokemon.com/assets/cms2-fr-fr/pdf/trading-card-game/rulebook/xy8-rulebook-fr.pdf)
    - mode distant temps réel et IA
- technologies :
    - langages : HTML5 / CSS3 / Javascript
    - technologies : canvas, websockets et mongodb (base de données)
    - frameworks et bibliothèques : nodejs, jade, bootstrap
    - environnement de développement : webstorm

# Thème 2 (Julien Dehos)

- développement d’un jeu en réseau (morpion, puissance 4, pong...)
- points importants :
    - architecture réseau (client-serveur ou peer-to-peer)
    - interface graphique
- technologies :
    - C++
    - SFML


# Dates importantes

date | objet |
--- | --- |
lundi 23 mai 2016 | réunion de présentation  (13h-13h15, salle 1) |
lundi 6 juin 2016 | début des projets (salles 111 et 113) |
lundi 6 juin 2016 | CM1 (9h-10h, amphi B) |
mercredi 8 juin 2016  | CM2 (9h-10h, amphi B) |
vendredi 10 juin 2016 | rendre le cahier des charges |
mercredi 15 juin 2016 | livrer un prototype |
mardi 21 juin 2016  | CM3 (9h-10h, amphi B) |
mercredi 22 juin 2016 | livrer le projet et rendre le rapport |
vendredi 24 juin 2016 | soutenances et fin des projets |

#include ../soutenances_2015-2016.md

