WWW_ROOT = ~/public-html/L3Info_Projets

.PHONY: all publish clean

all: $(WWW_ROOT)
	$(MAKE) -C www WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C CM1 WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C CM2 WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C CM3 WWW_ROOT=$(WWW_ROOT)

$(WWW_ROOT):
	mkdir -p $(WWW_ROOT)

publish: all
	scp -r $(WWW_ROOT) diran.univ-littoral.fr:public-html/

clean:
	rm -rf $(WWW_ROOT)
	$(MAKE) -C www clean
	$(MAKE) -C CM1 clean
	$(MAKE) -C CM2 clean
	$(MAKE) -C CM3 clean

