---
title: "Projets, CM1"
date: 6 juin 2016
---

# Introduction

## Motivation du module Projets

- réaliser un « vrai » logiciel est souvent complexe voire compliqué
- il existe de nombreuses méthodologies et nombreux outils
- mais pas de miracle
- objectif du module : réaliser un logiciel pas complètement trivial en utilisant quelques méthodes et outils classiques

## Organisation du module

- du 6 au 24 juin 2016 à temps plein
- présence **OBLIGATOIRE** : 9h-12h + 13h-16h
(en salle de cours pour les CM, en salles machines le reste du temps)
- à réaliser : cahier des charges, livrables, rapport, soutenance
- informations : [https://www-lisic.univ-littoral.fr/~dehos](https://www-lisic.univ-littoral.fr/~dehos)

## Objectifs de la gestion de projet

- faire aboutir le projet
- respecter les fonctionnalités demandées, les délais et les coûts
- prévoir les problèmes possibles, détecter les problèmes réels

## Étapes d’un projet

- analyse du besoin
- conception, développement, validation, documentation
- livraison, déploiement, maintenance

$\rightarrow$ déroulement unitaire ou itératif

## "Fil rouge" : le client et la qualité !

- répondre au besoin effectif de l'utilisateur (ou du client)
- adopter des normes de codage
- utiliser des méthodes et des outils de développement performants
- construire une organisation efficaces (bilan des compétences, pair-programming, ...)

# Premières étapes d'un projet informatique

## Analyse du besoin

- objectif :
    - savoir ce que veut exactement le client
    - voir si c’est faisable
- outils :
    - discussion avec le client
    - étude de l’existant
    - maquette, prototype, ...

## Spécification

- définition précise du résultat attendu
- « contrat » entre le client et le prestataire (cahier des charges)

## Gestion des risques

- objectif : prévoir les problèmes possibles et savoir les gérer
- identifier les risques :
    - type (complexité, incertitude)
    - domaine (cible, projet)
- prévoir les risques :
    - impact, probabilité d’apparition
    - prévention, correction
- suivre les risques : les détecter quand ils se produisent
- choix d’une stratégie de développement :
    - unitaire si le projet est simple et certain
    - itératif si complexe ou incertain

## Organisation du projet

- déterminer comment on va développer le logiciel
- ( analyse des coûts : établir une estimation du prix du projet )
- planification : établir un calendrier de développement
- assurance qualité du logiciel : déterminer les actions qui permettront de s'assurer de la qualité du produit fini
- répartition des tâches : hiérarchiser les tâches et sous-tâches nécessaires au développement du logiciel

## Planification

- objectif : prévoir le déroulement, le vérifier, adapter si besoin
- organiser le projet : étapes, durées, dépendances, ressources
- planifier : affectation réelle des dates et ressources
- suivre : comparer prévu/réel, détecter les problèmes, actions correctrices

## Conception

- architecture : quelles sont les différentes parties de l'application ?
- interface : comment les différentes parties interagissent ?
- premiers choix techniques (librairies, frameworks, ...)

## Implémentation

- produire du code
- gérer la coordination de la production (versionning, intégration, ...)

## Tests

- essayer le logiciel sur des données d'exemple pour s'assurer qu'il fonctionne correctement
- tests unitaires : faire tester les parties du logiciel par leurs développeurs
- tests d'intégration : tester pendant l'intégration
- tests de validation : pour acceptation par le client
- tests système : tester dans un environnement proche de l'environnement de production

## Modèles de développement - spirale

- Production du logiciel par itération ; une itération = une version améliorée du logiciel ; centré sur la gestion des risques

![](spiral_model.png)

## Modèles de développement - itératif

- Production du logiciel par itération ; une itération = un produit (un artéfact)

![](Iterative_model.png)


# Cahier des charges

## Motivation

- formalise la demande, décrit ce que le prestataire doit fournir au client à la fin du projet
- « contrat » entre le client et le prestataire
- doit être précis et exhaustif
- et si possible clair et concis ...

$\rightarrow$ voir l’[exemple fourni](https://gogs.univ-littoral.fr/jdehos/cours_projets_l3/src/master/template_cdc)

## Contenu

- fiche de renseignements
- description du besoin
- spécifications
- annexes

## Fiche de renseignements

- nom et objet du projet
- noms du client et du prestataire
- dates de début et de fin du projet

## Description du besoin

- objectif : présenter la demande
- contenu : contexte, besoins, priorités
- forme : texte

## Spécifications

- objectif : formaliser les livrables/fonctionnalités demandés
- contenu : caractéristiques précises et exhaustives
- forme : listes

## Annexes

- maquettes
- formats de fichiers, protocoles de communication
- diagrammes de cas d’utilisation
- planning prévisionnel
- ...

# Travail à réaliser

## Dans la semaine

- rédiger un cahier des charges **validé par le « client »**
- définir les tâches
- construire l'architecture globale de l'application