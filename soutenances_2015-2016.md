
# Soutenances projets 2015-2016

date : vendredi 24 juin 2016

salle : C102

jury : Virginie MARION-POTY, Fabien TEYTAUD, Julien DEHOS

horaire | groupe | projet |
---|---|---|
13h-13h15 | Tony DEBORGUERE, Baris TEKELI, Malik TOUAT, Zhiwen QI| Space Wars |
13h30-13h45 | Baptiste FAMCHON, Eric SAILLY, Kossi EHO | Battleship |
14h-14h30 | Sébastien SCHOUTEETEN, Sifa TEKELI, Aurélien HARINCK, Kevin WALLOIS, Alexis JOUIN, Maxence HABINKA | Pokemon |
14h45-15h | Anthony BOENS, Bilel ALOUI, Alexis VERHAEGHE | UNO |
15h15-15h45 | François BOMY, Lucas BOURRÉ, Clémence BUCHE, Thomas HAYAERT, Benjamin HEQUET, Alexandre LEBLANC, Nathan PECQUEUX, Pierre PREUSS | Pokemon |
16h-16h15 | Ilias AFRASS, Marouane ABAKARIM, Mohamed ADANSAR, Anas TAIBI | Pong |
16h30-16h45 | Othmane HADDOU, Youssef BENHARROUS | Morpion |

