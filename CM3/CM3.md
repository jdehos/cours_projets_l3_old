---
title: "Projets, CM3"
date: 2016-05-18
---

# Dernières étapes d’un projet informatique

## Problématique

- le projet ne termine pas à l’écriture de la dernière ligne de code
- il reste encore à effectuer/terminer d’autres étapes avant de livrer le projet
- conseil : mieux vaut un logiciel partiel bien validé/livré qu’un logiciel complet mal validé/livré
- d’où l’importance de planifier/suivre le projet pour voir si on peut finir dans les délais ou s’il faut sacrifier des spécifications

## Étape de validation

- objectif : vérifier que le logiciel répond aux spécifications
- comment ? $\rightarrow$ tests unitaires, cas d’utilisation, ...
- évidemment on valide régulièrement au cours du projet mais il faut tout revérifier sur le logiciel final

## Documentation

- objectif : expliquer comment utiliser le logiciel et comment maintenir le code source
- types de doc :
    - commentaires de code
    - manuel d’installation
    - manuel d’utilisation
    - documentation de maintenance
- certaines documentations sont faites pendant le développement mais d’autres nécessitent d’avoir le logiciel à peu près fini

## Release

- quoi-qu’est-ce : le logiciel final avec tout ce qu’il faut pour pouvoir l’utiliser
- forme : une archive tar gz (par exemple) ou un tag dans le système de gestion de versions
- contenu :
    - selon le CDC : code source avec script de compilation/installation ou binaire compilé pour les plates-formes prévues
    - documentation
    - éventuellement : fichiers de configuration, données d’exemple, ...

## Autres étapes possibles

- déploiement (installation chez le client, migration de données...)
- formation des utilisateurs, assistance technique
- maintenance (correction de bugs, ajout de fonctionnalités)

## Bilan

- faire la synthèse des spécifications et de la planification réellement obtenues
- comparer avec les prévisions initiales
- faire le bilan de ce qui a fonctionné ou non et y penser pour les prochains projets


# Rapport de projet

## Le rapport de projet dans la vraie vie

- bilan pour garder une trace (archives) et pour progresser (projets futurs)
- complété par la documentation (installation, utilisation, maintenance)

## Le rapport de projet à la fac

- savoir présenter le travail réalisé (clairement et objectivement)
- savoir prendre du recul

## Public visé par le rapport

- connait le domaine (développement informatique)
- mais pas le projet ni son contexte
- importance d’être synthétique et clair

## Contenu

- présentation du projet :
    - contexte
    - besoins
    - spécifications demandées
    - l’état du produit (logiciel) au début du projet
- réalisation :
    - présentation du logiciel réalisé
    - présentation technique (architecture générale, points importants)
- bilan :
    - déroulement du projet (prévisions, problèmes rencontrés, solutions, ...)
    - résumé des objectifs réalisés (ou pas)
    - conclusion pour les projets futurs

## Forme

- document PDF (cf [template fourni](https://gogs.univ-littoral.fr/jdehos/cours_projets_l3/src/master/template_rapport))
- texte + illustrations (captures écrans, UML, schémas, ...)
- pas de code source
- faire simple, concis et structuré
- corriger l’orthographe et la conjugaison


# Soutenance de projet

## Objectif

- idem rapport : présenter projet et résultats
- même type de public : connait le domaine mais pas le projet
- forme différente : oral + support

## Contenu

- similaire au rapport :
    - présenter le projet et la demande
    - travaux réalisés
    - bilan, conclusion
- soigner introduction/conclusion (progression, prise de recul)
- présenter tous les travaux mais n’en détailler que quelques uns

## Forme (classiquement)

- 15 minutes (maximum) de présentation + questions
- accès à un vidéo projecteur + ordinateur
- si PC portable perso, être sûr du multi-écran et de la batterie

## Quelques conseils sur les slides

- limiter le nombre de slides à un par minute max
- ne pas surcharger un slide : liste des idées à exprimer et/ou illustrations
- « un bon schéma au lieu d’un long texte »
- sur chaque slide : titre de la présentation, auteurs, numéro du slide
- toujours prévoir une version PDF sur clé USB au cas où
- éviter les animations inutiles !

## Quelques conseils sur la démonstration

- objectif : montrer le logiciel en train de fonctionner
- intéressant si aspect dynamique particulier ou pour éviter la monotonie
- démo en direct dangereuse, vidéo pré-enregistrée plus sûre
- prévoir/structurer/commenter ce qui est montré

## Quelques conseils pour l’oral

- rester objectif : ne pas sur-vendre le travail, ni le dévaloriser
- éviter la feuille anti-sèche (les slides doivent suffire)
- plusieurs orateurs : bien répartir, ne pas se couper/contredire
- **faire une répétition** (voire plusieurs)


# Travail à réaliser

## À la fin du projet 

- livrer le projet (release propre et complète)
- rendre un rapport
- faire une soutenance

